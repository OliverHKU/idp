package admin

import (
       _ "github.com/ziutek/mymysql/thrsafe"
       "idp/dbconnector"
       "idp/time0"
       "idp/structs"
       "strconv"
)

func InsertCar (uid int, pid string, ptp string) {
     db := dbconnector.Connect()
     dbconnector.CheckedResult(db.Query("insert into car (user_id, plate_id, plate_type) values (%d, '%s', '%s')", uid, pid, ptp))
     dbconnector.Close(&db)
}

func MakeReservation (uid int, stime string, etime string) {
     db := dbconnector.Connect()
     dbconnector.CheckedResult(db.Query("insert into registration (user_id, start_time, end_time) values (%d, '%s', '%s')", uid, stime, etime))
     dbconnector.Close(&db)
}

func MakeLog (pid string, accepted bool) {
	db := dbconnector.Connect()
	rows, _, _ := db.Query("select * from car where plate_id = '%s'", pid)
	if rows == nil {
		dbconnector.Close(&db)
		return
	}
	currentTimeSql := time0.CurrentTimeSql()
	uid, _ := strconv.Atoi(string(rows[0][0].([]byte)))
	ptp := string(rows[0][2].([]byte))
	ack := ""
	if accepted == true {
		ack = "true"
	} else { ack = "false" }
	dbconnector.CheckedResult(db.Query("insert into log (log_time, user_id, plate_id, plate_type, accepted) values ('%s', %d, '%s', '%s', '%s')", currentTimeSql, uid, pid, ptp, ack))
	dbconnector.Close(&db)
}

func DeleteCar (uid int) {
     db := dbconnector.Connect()
     //rows, _, _ := db.Query("select * from car where user_id = %d", uid)
     //uid, _ := strconv.Atoi(string(rows[0][0].([]byte)))
     dbconnector.CheckedResult(db.Query("delete from car where user_id = %d", uid))
     dbconnector.CheckedResult(db.Query("delete from registration where user_id = %d", uid))
     dbconnector.Close(&db)
}

func DeleteReservation (rid int) {
     db := dbconnector.Connect()
     dbconnector.CheckedResult(db.Query("delete from registration where reg_id = %d", rid))
     dbconnector.Close(&db)
}

func ChangeTime (uid int, stime string, etime string) {
     db := dbconnector.Connect()
     dbconnector.CheckedResult(db.Query("update registration set start_time = '%s', end_time = '%s' where user_id = %d", stime, etime, uid))
     dbconnector.Close(&db)
}

func ChangePlate (uid int, pid string, ptp string) {
     db := dbconnector.Connect()
     dbconnector.CheckedResult(db.Query("update car set plate_id = '%s', plate_type = '%s' where user_id = %d", pid, ptp, uid))
     dbconnector.Close(&db)
}

func ShowCar (uid int) structs.Car {
     db := dbconnector.Connect()
     rows, _, _ := db.Query("select * from car where user_id = %d", uid)
     var car structs.Car
     car.UID = uid
     car.PID = string(rows[0][1].([]byte))
     car.PTP = string(rows[0][2].([]byte))     
     dbconnector.Close(&db)
     return car
}

func ShowReservation (uid int) structs.Reservation  {
     db := dbconnector.Connect()
     var res structs.Reservation
     rows, _, _ := db.Query("select * from registration where user_id = %d", uid)
     res.UID = uid
     rid, _ := strconv.Atoi(string(rows[0][0].([]byte)))
     res.RID = rid
     res.STime = string(rows[0][2].([]byte))
     res.ETime = string(rows[0][3].([]byte))
     dbconnector.Close(&db)
     return res
}

func ListCars () []structs.Car {
	db := dbconnector.Connect()
	var cars []structs.Car
	rows, _, _ := db.Query("select * from car")
	for k, _ := range rows {
		var car structs.Car
		uid, _ := strconv.Atoi(string(rows[k][0].([]byte)))
		car.UID = uid
		car.PID = string(rows[k][1].([]byte))
		car.PTP = string(rows[k][2].([]byte))
		cars = append(cars, car)
	}
	dbconnector.Close(&db)
	return cars
}

func ListReservations () []structs.Reservation {
	db := dbconnector.Connect()
	var ress []structs.Reservation
	rows, _, _ := db.Query("select * from registration")
	for k, _ := range rows {
		var res structs.Reservation
		rid, _ := strconv.Atoi(string(rows[k][0].([]byte)))
		uid, _ := strconv.Atoi(string(rows[k][1].([]byte)))
		res.RID = rid
		res.UID = uid
		res.STime = string(rows[k][2].([]byte))
		res.ETime = string(rows[k][3].([]byte))
		ress = append(ress, res)
	}
	dbconnector.Close(&db)
	return ress
}

func ShowLog (logTime string) structs.LogItem {
	db := dbconnector.Connect()
	var logitem structs.LogItem
	rows, _, _ := db.Query("select * from log where log_time = '%s'", logTime)
	logitem.LTime = logTime
	uid, _ := strconv.Atoi(string(rows[0][1].([]byte)))
	logitem.UID = uid
	logitem.PID = string(rows[0][2].([]byte))
	logitem.PTP = string(rows[0][3].([]byte))
	ack := string(rows[0][4].([]byte))
	if ack == "true" {
		logitem.ACK = true
	} else {
		logitem.ACK = false
	}
	dbconnector.Close(&db)
	return logitem
}

func ListLogs () []structs.LogItem {
	db := dbconnector.Connect()
	var logs []structs.LogItem
	rows, _, _ := db.Query("select * from log")
	for k, _ := range rows {
		var logitem structs.LogItem
		logitem.LTime = string(rows[k][0].([]byte))
		uid, _ := strconv.Atoi(string(rows[k][1].([]byte)))
		logitem.UID = uid
		logitem.PID = string(rows[k][2].([]byte))
		logitem.PTP = string(rows[k][3].([]byte))
		ack := string(rows[k][4].([]byte))
		if ack == "true" {
			logitem.ACK = true
		} else {
			logitem.ACK = false
		}
		logs = append(logs, logitem)
	}
	dbconnector.Close(&db)
	return logs
}
