package main

import (
	"fmt"
	"os/exec"
	"strings"
	"bytes"
	"log"
	"time"
	"idp/dbconnector"
	"strconv"
	"idp/time0"
	"idp/admin"
)

func getPlate(input string) string {
	i := strings.Index(input, "-")
	if i == -1 {
		return ""
	}
	subStr := input[i+2:i+10]
	return strings.TrimSpace(subStr)
}

func authorize(plate string) bool {
	/*if plate == "LTM378" {
		return true
	}
	return false*/
	db := dbconnector.Connect()
	rows, _, _ := db.Query("select user_id from car where plate_id = '%s'", plate)
	if rows == nil {
		dbconnector.Close(&db)
		return false
	}
	uid, _ := strconv.Atoi(string(rows[0][0].([]byte)))
	rows, _, _ = db.Query("select * from registration where user_id = %d", uid)
	currentTimeSql := time0.CurrentTimeSql()
	startTime := string(rows[0][2].([]byte))
	endTime := string(rows[0][3].([]byte))
	if time0.TimeDiffSql(startTime, currentTimeSql)>0 || time0.TimeDiffSql(currentTimeSql, endTime)>0 {
		dbconnector.Close(&db)
		return false
	}
	dbconnector.Close(&db)
	return true
}

func logdb(plate string, auth bool) {
	admin.MakeLog(plate, auth)
}

/*func reportArduino(plate string, auth bool) {

}*/

func openDoor() {
	_, err := exec.Command("python", "door.py").Output()
	if err != nil {
		fmt.Println("Error opening door")
		log.Fatal(err)
	}
}

/*func deleteFiles() {
	_, err := exec.Command("rm", "-r", "motion_detect/img").Output()
	_, err0 := exec.Command("mkdir", "-p", "motion_detect/img").Output()
	if err != nil || err0 != nil {
		fmt.Println("Error deleting files")
		log.Fatal(err)
	}
}*/

func checkErr(err error, msg string) {
	if err != nil {
		fmt.Println(msg)
	}
}

func main() {
	var lastPlate string
	for {
		cmd := exec.Command("ls", "motion_detect/img")
		var out bytes.Buffer
		cmd.Stdout = &out
		err := cmd.Run()
		checkErr(err, "Error listing img files")
		fileLines := strings.Split(out.String(), "\n")
		//fmt.Println("Before printing num of imgs")
		//fmt.Println(len(fileLines))
		//fmt.Println("After printing num of imgs")
		if len(fileLines) < 1 {
			time.Sleep(time.Second)
			continue
		}
		for i, temp := range fileLines {
			if i < len(fileLines)-1 {
				filePath := fmt.Sprintf("motion_detect/img/%v", temp)
				out, err := exec.Command("alpr", filePath).Output()
				checkErr(err, "Error calling alpr")
				outString := string(out[:])
				plate := getPlate(outString)
				if plate != "" && plate != lastPlate {
					lastPlate = plate
					
					// pass plate and timestamp to authorization
					auth := authorize(plate)

					// open door if authorization passed
					if auth == true {
						fmt.Println("Door opening!!!!!!!!!!!!!!!!!!!!!!!")
						go openDoor()
					}
					
					// log to database (optional)
					logdb(plate, auth)

					// report to front pannel
					//reportArduino(plate, auth)

					fmt.Println(plate)

					// backup file
					_, err = exec.Command("cp", filePath, "motion_detect/backup_img").Output()
					checkErr(err, "Error backing up file")
				}
				// recycle file
				_, err = exec.Command("rm", filePath).Output()
				checkErr(err, "Error recycling file")
			}
		}
		//time.Sleep(time.Second)
	}
}
