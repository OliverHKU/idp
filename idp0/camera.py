from picamera import PiCamera
from time import sleep
from datetime import datetime

camera = PiCamera()

camera.start_preview()
sleep(2)
file_name = datetime.now().strftime('./%Y%B%d_%I:%M%p.jpg')
camera.capture(file_name)
camera.stop_preview()
