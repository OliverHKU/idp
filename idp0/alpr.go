package main

import (
	"fmt"
	"os/exec"
	"log"
)

func getPlate(fileName string, country string) []byte {
	out, err := exec.Command("alpr", "-c", country, fileName).Output()
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Printf("************** Result Is **************\n%s", out)
	return out
}

func main() {
	fileName := "h786poj.jpg"
	country := "eu"
	out := getPlate(fileName, country)
	fmt.Printf("************** Result Is **************\n%s", out)
}
