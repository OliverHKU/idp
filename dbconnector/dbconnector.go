package dbconnector

import (
       "fmt"
       "github.com/ziutek/mymysql/autorc"
       "github.com/ziutek/mymysql/mysql"
       _ "github.com/ziutek/mymysql/thrsafe"
)

// checkError and checkedResult are help functions for debugging

func checkError(err error) {
     if err != nil {
     	fmt.Printf("[dbconnector] Error from SQL server: %v\n", err)
				  //os.Exit(1)
				  }
}


// Check result for sql query
func CheckedResult(rows []mysql.Row, res mysql.Result, err error) ([]mysql.Row, mysql.Result) {
     checkError(err)
     return rows, res
}


// Connect connects the program to database
func Connect() autorc.Conn{
     user := "root"
     pass := "0"
     dbname := "garage"

     proto := "tcp"
     addr := "127.0.0.1:3306"

     db := *autorc.New(proto, "", addr, user, pass, dbname)
     
     fmt.Printf("Connect to %s:%s... ", proto, addr)
     //checkError(db.Connect())
     /*err := db.Connect()
     if err != nil {
     	fmt.Printf("[dbconnector.Connect] Error from SQL server: %v\n", err)
	}
	fmt.Println("OK")*/

	return db
}

func Close(db *autorc.Conn) {
     fmt.Println("Closing connection...")
     //checkError((*db).Close())
     err := (*db).Raw.Close()
     if err != nil {
     	fmt.Printf("[dbconnector.Close] Error from SQL server: %v\n", err)
	}
}

func ClearTable(table string) {
     db := Connect()
     CheckedResult(db.Query("truncate table %s", table))
     Close(&db)
}