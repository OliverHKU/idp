package main

import (
	"fmt"
	"html/template"
	"net/http"
	"regexp"
	"idp/admin"
	"strconv"
)

type Tuple struct {
	UID    int
	PID    string
	TStart string
	TEnd   string
	TLog   string
	ACC    string
}

type Page struct {
	Title string
	Body string
	Text Tuple
	Data []Tuple
}

var templates = template.Must(template.ParseFiles("views/edit.html", "views/view.html", "views/reg.html", "views/log.html", "views/index.html"))

var validPath = regexp.MustCompile("^/(index|edit|reg|view|save|delete|update|log)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)$")

func loadPage(ty string, title string) (*Page, error) {
	var body string
	if ty == "cars" {
		body = "This is me"
	}
	return &Page{Title: title, Body: body}, nil
}

func loadRegPage(ty string, title string) (*Page, error) {
	var body string	
	return &Page{Title: title, Body: body}, nil
}
 
func handler(w http.ResponseWriter, r *http.Request) {
     fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
     err := templates.ExecuteTemplate(w, tmpl+".html", p)
     if err != nil {
     	http.Error(w, err.Error(), http.StatusInternalServerError)
     }
}

func makeHandler(fn func (http.ResponseWriter, *http.Request, string, string)) http.HandlerFunc {
     return func(w http.ResponseWriter, r *http.Request) {
		 m := validPath.FindStringSubmatch(r.URL.Path)
		 if m == nil {
			 http.NotFound(w, r)
			 return
		 }
		 fn(w, r, m[2], m[3])
     }
}

// Handlers

func viewHandler(w http.ResponseWriter, r *http.Request, ty string, title string) {
	/*p, err := loadPage(ty, title)
	if err != nil {
     	http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}*/
	p, err := loadRegPage(ty, title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	(*p).Title = title
	//(*p).Data = make()
	cars := admin.ListCars()
	(*p).Data = make([]Tuple, 0, len(cars))
	
	for _, car := range cars {
		var t Tuple
		t.UID = car.UID
		t.PID = car.PID
		res := admin.ShowReservation(car.UID)
		t.TStart = res.STime
		t.TEnd = res.ETime
		(*p).Data = append((*p).Data, t)
	}
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, ty string, title string) {
	p, err := loadRegPage(ty, title)
	if err != nil {
     	p = &Page{Title: title}
	}
	fmt.Printf("[edit car] Title: %v\n", title)
	(*p).Title = title
	uid, _ := strconv.Atoi(title)
	fmt.Printf("[edit car] User id: %v\n", uid)
	car := admin.ShowCar(uid)
	(*p).Text.UID = uid
	(*p).Text.PID = car.PID
	res := admin.ShowReservation(car.UID)
	(*p).Text.TStart = res.STime
	(*p).Text.TEnd = res.ETime
	renderTemplate(w, "edit", p)
}

func regHandler(w http.ResponseWriter, r *http.Request, ty string, title string) {
	p, err := loadRegPage(ty, title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "reg", p)
	//http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func saveHandler(w http.ResponseWriter, r *http.Request, ty string, title string) {
	//fmt.Println("Save handler called")
	uid, _ := strconv.Atoi(r.FormValue("uid"))
	pid := r.FormValue("pid")
	stime := r.FormValue("stime")
	etime := r.FormValue("etime")
	admin.InsertCar(uid, pid, "us")
	admin.MakeReservation(uid, stime, etime)
	http.Redirect(w, r, "/reg/car/"+title, http.StatusFound)
}

func updateHandler(w http.ResponseWriter, r *http.Request, ty string, title string) {
	uid, _ := strconv.Atoi(r.FormValue("uid"))
	pid := r.FormValue("pid")
	stime := r.FormValue("stime")
	etime := r.FormValue("etime")
	admin.ChangePlate(uid, pid, "us")
	admin.ChangeTime(uid, stime, etime)
	http.Redirect(w, r, "/view/car/all", http.StatusFound)
}

func deleteHandler(w http.ResponseWriter, r *http.Request, ty string, title string) {
	uid, _ := strconv.Atoi(title)
	admin.DeleteCar(uid)
	http.Redirect(w, r, "/view/car/all", http.StatusFound)
}

func logHandler(w http.ResponseWriter, r *http.Request, ty string, title string) {
	p, err := loadRegPage(ty, title)
	if err != nil {
        p = &Page{Title: title}
	}
	(*p).Title = title
    logs := admin.ListLogs()
    (*p).Data = make([]Tuple, 0, len(logs))

    for _, log := range logs {
        var t Tuple
        t.UID = log.UID
        t.PID = log.PID
        t.TLog = log.LTime
        if log.ACK == true {
			t.ACC = "YES"
		} else {
			t.ACC = "NO"
		}
        (*p).Data = append((*p).Data, t)
    }
    renderTemplate(w, "log", p)
}

func indexHandler(w http.ResponseWriter, r *http.Request, ty string, title string) {
	p, err := loadRegPage(ty, title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "index", p)
}

func main() {
	//http.HandleFunc("/", makeHandler(indexHandler))
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/reg/", makeHandler(regHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))
	http.HandleFunc("/delete/", makeHandler(deleteHandler))
	http.HandleFunc("/update/", makeHandler(updateHandler))
	http.HandleFunc("/log/", makeHandler(logHandler))
	http.HandleFunc("/index/", makeHandler(indexHandler))
	http.ListenAndServe(":8080", nil)
}
