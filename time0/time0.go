package time0

import (
       "time"
       "fmt"
)



func CurrentTimeSql() string {
     currentTime := time.Now()
     return currentTime.String()[:19]
}

// TimeDiffSql takes two strings formated as mysql datetime, and returns the representation of time1 minus time2 (the time difference) in terms of seconds
func TimeDiffSql(time1 string, time2 string) int {
     var result int
     result = 0
     const form = time.RFC3339
     date1 := time1[:10]
     date2 := time2[:10]
     clock1 := time1[11:]
     clock2 := time2[11:]
     string1 := date1 + "T" + clock1 + "Z"
     string2 := date2 + "T" + clock2 + "Z"
     time1Go, err1 := time.Parse(form, string1)
     time2Go, err2 := time.Parse(form, string2)
     if err1 != nil {fmt.Println(err1)}
     if err2 != nil {fmt.Println(err2)}
     duration := time1Go.Sub(time2Go)
     result = int(duration/1000000000)
     return result
}