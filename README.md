# README #

Please follow the steps to launch the IDP Smart Garage System (IDPSGS).

## What is IDPSGS? ##

* IDPSGS stands for Integrated Design Project Smart Garage System. It is part of a university smart home prototyping project under a course Integrated Design Project. It provides a solution to manage the control of garages in an efficient way using Raspberry Pi. The project is designed for house owners or event managers who need to provide garage spaces for visitors and/or guests.
* [Introductory Video](https://youtu.be/Dwb1PnJWI7I)

* The workflow:
    * Guest registration: car plate of guests can be registered together with event time and duration via an online platform.
    * Garage door surveillance: a surveillance and motion-detection system based on OpenCV and PiCamera.
    * Automatic car plate recognition: a system based on OpenAlpr which recognizes car plates in photos.
    * Automatic door control: The opening and closing of the garage door are controlled by the car plate recognition system.
    * Logging: Any accepted or denied access to the garage will be logged, which can be checked via the web app.
* Version: 0.9

## How do I get set up? ##

### Summary of set up ###
IDPSGS is developed using Go and Python, thus go and python need to be installed. The Python OpenCV module cv2 should also be installed. The following instructions are tested on Ubuntu MATE installed on Raspberry Pi 3.
### Configuration ###
1. Make sure Python is installed. run ```which python``` to check.
2. Install cv2 module. You can follow [this](http://stackoverflow.com/questions/3325528/how-to-install-opencv-for-python).
3. Install Go. Follow [this](https://golang.org/doc/install). Make sure your GOPATH is set.
4. ```cd``` into your $GOPATH/src and do ```git clone git@bitbucket.org:OliverHKU/idp.git``` to download this project.
5. Follow [this](https://github.com/ziutek/mymysql) instruction to download the MySQL client API for Go under your GOPATH/src. This is used by IDPSGS to securely access the database.
6. Make sure mysql server is installed. run ```which mysql``` to check. Run ```mysql -u{{Your User Name}} -p{{Your Password}}```. The default username is 'root' and password is '0'. To change the setting, modify the function Connect() in dbconnector/dbconnector.go.
7. In mysql command line, issue ```create database idp;```, then ```quit;```.
8. Load sample data into database: ```mysql -uroot -p0 < dump.sql.
9. Compile and launch the web app: 
  ```
  go build server.go
  ./server  
  ```
  It can be accessed via [localhost:8080/index/see/options](localhost:8080/index/see/options)
10. Compile and run the car plate recognition service:
  ```
  go build recognize.go
  ./recognize
  ```
11. Run the surveillance script with ```python motion_detect/pi_surveillance.py -conf conf.json```.

## Reference ##
1. [PiImageSearch](http://www.pyimagesearch.com/2015/06/01/home-surveillance-and-motion-detection-with-the-raspberry-pi-python-and-opencv/)
2. [OpenAlpr](https://github.com/openalpr/openalpr)
3. [mymysql](https://github.com/ziutek/mymysql)

## Contact Me ##

* [Oliver Li](mailto:oliver.li@tum.de)