package structs

type Car struct {
     UID 	int
     PID	string
     PTP	string
}

type Reservation struct {
     RID	 int
     UID	 int
     STime	 string
     ETime	 string
}

type LogItem struct {
     LTime	string
     UID	int
     PID	string
     PTP	string
     ACK	bool
}
